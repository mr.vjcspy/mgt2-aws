<?php
/**
 * Created by PhpStorm.
 * User: xuantung
 * Date: 10/23/18
 * Time: 4:02 PM
 */

namespace SM\PWABanner\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\App\Config\ScopeConfigInterface;

class ChangeBannerSetting implements \Magento\Framework\Event\ObserverInterface
{


    /**
     * @var \SM\PWABanner\Helper\Data
     */
    protected $helper;
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var \SM\PWABanner\Model\BannerFactory
     */
    protected $bannerFactory;

    /**
     * ChangeBannerSetting constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param \SM\PWABanner\Helper\Data $helper
     * @param \SM\PWABanner\Model\BannerFactory $bannerFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \SM\PWABanner\Helper\Data $helper,
        \SM\PWABanner\Model\BannerFactory $bannerFactory)
    {
        $this->helper = $helper;
        $this->scopeConfig = $scopeConfig;
        $this->bannerFactory = $bannerFactory;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $config_active_banners = intval($this->scopeConfig->getValue("pwa/banner/pwa_banner_active"));
        $current_active_banners = $this->helper->numberOfActiveBanners();
        if($config_active_banners < $current_active_banners) {
            $collection = $this->bannerFactory->create()->getCollection()
                ->addFieldToFilter('is_active', \SM\PWABanner\Model\Status::STATUS_ACTIVED)
                ->setOrder('created_at', \Magento\Framework\Data\Collection::SORT_ORDER_DESC);

            $i = 0;
            foreach ($collection as $banner) {
                if($i < abs($config_active_banners - $current_active_banners))
                    $banner->setIsActive(false)->save();
                $i++;
            }
        }

    }
}