<?php
/**
 * Created by PhpStorm.
 * User: xuantung
 * Date: 10/6/18
 * Time: 3:15 PM
 */

namespace SM\PWABanner\Repositories;


use SM\Core\Api\Data\PWABanner;
use SM\XRetail\Repositories\Contract\ServiceAbstract;

class PWABannerManagement extends ServiceAbstract
{
    /**
     * @var \SM\PWABanner\Model\ResourceModel\BannerFactory
     */
    protected $_bannerFactory;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;
    /**
     * PWABannerManagement constructor.
     * @param \Magento\Framework\App\RequestInterface $requestInterface
     * @param \SM\XRetail\Helper\DataConfig $dataConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \SM\PWABanner\Model\BannerFactory $bannerFactory
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $requestInterface,
        \SM\XRetail\Helper\DataConfig $dataConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \SM\PWABanner\Model\BannerFactory $bannerFactory)
    {
        $this->_bannerFactory = $bannerFactory;
        $this->storeManager = $storeManager;
        parent::__construct($requestInterface, $dataConfig, $storeManager);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getBannerData() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $fileName = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'banner';

        $items      = [];
        $collection = $this->getBannerCollection($this->getSearchCriteria());
        if ($collection->getLastPageNumber() < $this->getSearchCriteria()->getData('currentPage')) {
        }
        else {
            foreach ($collection as $banner) {
                $banners = new PWABanner();
                $banners->addData(
                    [
                        'banner_id'   => $banner->getId(),
                        'banner_url' => $fileName.$banner->getData('banner_url'),
                        'is_active' => $banner->getData('is_active'),
                    ]);
                $items[] = $banners;
            }
        }

        return $this->getSearchResult()
            ->setSearchCriteria($this->getSearchCriteria())
            ->setItems($items)
            ->setTotalCount($collection->getSize())
            ->getOutput();
    }

    public function getBannerCollection($searchCriteria) {
        /** @var \SM\PWABanner\Model\ResourceModel\Banner\Collection $collection */
        $collection = $this->_bannerFactory->create()->getCollection();
        $collection->addFieldToFilter('is_active', \SM\PWABanner\Model\Status::STATUS_ACTIVED);
        $collection->setCurPage(is_nan($searchCriteria->getData('currentPage')) ? 1 : $searchCriteria->getData('currentPage'));
        $collection->setPageSize(
            is_nan($searchCriteria->getData('pageSize')) ? DataConfig::PAGE_SIZE_LOAD_CUSTOMER : $searchCriteria->getData('pageSize')
        );

        return $collection;
    }
}