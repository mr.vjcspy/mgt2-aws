<?php

namespace SM\Sales\Model\ResourceModel;

class SellerOrder extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct() {
        $this->_init('sm_seller_order', 'id');
    }
}
