<?php

namespace SM\Sales\Model\ResourceModel\SellerOrder;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected function _construct() {
        $this->_init('SM\Sales\Model\SellerOrder', 'SM\Sales\Model\ResourceModel\SellerOrder');
    }
}
