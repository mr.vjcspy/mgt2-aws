<?php

namespace SM\Sales\Model;

class SellerOrder extends \Magento\Framework\Model\AbstractModel
    implements \SM\Sales\Api\Data\OrderSyncErrorInterface, \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'sm_seller_order';

    protected function _construct() {
        $this->_init('SM\Sales\Model\ResourceModel\SellerOrder');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
