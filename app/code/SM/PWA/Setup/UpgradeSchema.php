<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace SM\PWA\Setup;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    public function __construct(
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\App\State $state
    ) {
        $this->orderFactory = $orderFactory;
        try {
            $state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);
        }
        catch (LocalizedException $e) {
        }
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            $this->addPwaDataToOrder($setup, $context);
        }
    }

    protected function addPwaDataToOrder(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->getConnection()->dropColumn($installer->getTable('quote'), 'is_pwa');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order'), 'is_pwa');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order_grid'), 'is_pwa');

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'is_pwa',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'default' => '0',
                'comment' => 'Is enable PWA',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'is_pwa',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'default' => '0',
                'comment' => 'Is enable PWA',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'is_pwa',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'default' => '0',
                'comment' => 'Is enable PWA',
            ]
        );
        $setup->endSetup();
    }
}
