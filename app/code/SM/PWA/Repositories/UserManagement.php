<?php

namespace SM\PWA\Repositories;

use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\SecurityViolationException;
use SM\XRetail\Repositories\Contract\ServiceAbstract;
use SM\Core\Api\Data\XCustomer;
use SM\Core\Api\Data\CustomerAddress;

/**
 * Class UserManagement
 *
 * @package SM\PWA\Repositories
 */
class UserManagement extends ServiceAbstract {

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $_encryptor;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Magento\Customer\Api\GroupManagementInterface
     */
    protected $customerGroupManagement;

    /**
     * @var \SM\Customer\Repositories\CustomerManagement
     */
    protected $customerManagement;

    /**
     * @var \Magento\Customer\Model\CustomerExtractor
     */
    protected $customerExtractor;
    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManagement;
    /**
     * @var \SM\Customer\Helper\Data
     */
    protected $customerHelper;
    /**
     * @var \Magento\Newsletter\Model\SubscriberFactory
     */
    protected $subscriberFactory;
    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * @var \Magento\Customer\Api\Data\CustomerInterfaceFactory
     */
    protected $customerInterfaceFactory;
    /**
     * @var \Magento\Customer\Model\CustomerRegistry
     */
    protected $customerRegistry;

    /**
     * @var \SM\Integrate\Helper\Data
     */
    private $integrateHelper;

    /**
     * @var \SM\Integrate\Model\RPIntegrateManagement
     */
    protected $rpIntegrateManagement;

    /**
     * CustomerManagement constructor.
     *
     * @param \Magento\Framework\App\RequestInterface $requestInterface
     * @param \SM\XRetail\Helper\DataConfig $dataConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Api\GroupManagementInterface $customerGroupManagement
     * @param \SM\Customer\Repositories\CustomerManagement $customerManagement
     * @param \Magento\Customer\Model\CustomerExtractor $customerExtractor
     * @param \Magento\Customer\Api\AccountManagementInterface $accountManagement
     * @param \SM\Customer\Helper\Data $customerHelper
     * @param \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerInterfaceFactory
     * @param \Magento\Customer\Model\CustomerRegistry $customerRegistry
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $requestInterface,
        \SM\XRetail\Helper\DataConfig $dataConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Api\GroupManagementInterface $customerGroupManagement,
        \SM\Customer\Repositories\CustomerManagement $customerManagement,
        \Magento\Customer\Model\CustomerExtractor $customerExtractor,
        \Magento\Customer\Api\AccountManagementInterface $accountManagement,
        \SM\Customer\Helper\Data $customerHelper,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerInterfaceFactory,
        \SM\Integrate\Helper\Data $integrateHelperData,
        \SM\Integrate\Model\RPIntegrateManagement $RPIntegrateManagement,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry
    ) {
        $this->_encryptor              = $encryptor;
        $this->customerFactory         = $customerFactory;
        $this->customerRepository      = $customerRepository;
        $this->customerGroupManagement = $customerGroupManagement;
        $this->customerManagement      = $customerManagement;
        $this->customerExtractor       = $customerExtractor;
        $this->accountManagement       = $accountManagement;
        $this->customerHelper          = $customerHelper;
        $this->subscriberFactory       = $subscriberFactory;
        $this->dataObjectHelper        = $dataObjectHelper;
        $this->customerInterfaceFactory = $customerInterfaceFactory;
        $this->integrateHelper                = $integrateHelperData;
        $this->rpIntegrateManagement    = $RPIntegrateManagement;
        parent::__construct($requestInterface, $dataConfig, $storeManager);
        $this->customerRegistry = $customerRegistry;
    }

    public function LoginToPWA() {
        $data     = $this->getRequestData();
        $username = base64_decode($data['p2']);
        $password = base64_decode($data['p1']);
        $store = $data['storeId'];

        if (is_null($store))
            throw  new \Exception("Must have param storeId");

        /** @var \Magento\Customer\Model\CustomerFactory $customerFactory */
        $customerFactory = $this->customerFactory->create();
        $customerFactory->setWebsiteId($this->storeManager->getStore($store)->getWebsiteId());
        $canLogin            = $customerFactory->authenticate($username,$password);
        if ($canLogin) {
            $newTokenKey = md5(uniqid(rand(), true));

            $customer = $customerFactory->loadByEmail($username);


            $searchCriteria = new \Magento\Framework\DataObject(
                [
                    'storeId'   => $store,
                    'entity_id' => $customer->getData('entity_id'),
                    'email' => $username
                ]);

            return $this->loadDataCustomer($searchCriteria)->getOutput();
        }
        else {
            throw new \Exception("Invalid email or password. Please try again!");
        }
    }

    public function getRetailGuestCustomer() {
        $store = $this->getSearchCriteria()->getData('storeId');

        if (is_null($store))
            throw  new \Exception("Must have param storeId");

        $this->storeManager->setCurrentStore($store);

        $guestCustomer = $this->getDefaultCustomerId($store);

        if(!!$this->getSearchCriteria()->getData('customerId')){
            $guestCustomer = $this->getSearchCriteria()->getData('customerId');
        }
        $searchCriteria = new \Magento\Framework\DataObject(
            [
                'storeId'   => $store,
                'entity_id' => $guestCustomer
            ]);


        return $this->customerManagement->loadCustomers($searchCriteria)->getOutput();

    }

    private function getDefaultCustomerId($storeId) {
        try {
            $customer = $this->customerRepository->get(
                \SM\Customer\Helper\Data::DEFAULT_CUSTOMER_RETAIL_EMAIL,
                $this->storeManager->getStore($storeId)->getWebsiteId());
        }
        catch (\Exception $e) {
            $customer = null;
        }
        if (!is_null($customer) && $customer->getId()) {
            return $customer->getId();
        }
        else {
            $data = [
                "group_id"    => $this->customerGroupManagement->getDefaultGroup($storeId)->getId(),
                "email"       => \SM\Customer\Helper\Data::DEFAULT_CUSTOMER_RETAIL_EMAIL,
                "first_name"  => "Guest",
                "last_name"   => "Customer",
                "middle_name" => "",
                "prefix"      => "",
                "suffix"      => "",
                "gender"      => 0,
                "storeId"     => $storeId,
            ];

            return $this->customerManagement->create(['customer' => $data, 'storeId' =>  $storeId]);
        }
    }


    /**
     * @return array
     * @throws LocalizedException
     */
    public function createCustomerAccountViaPWA() {
        $customerData = $this->getRequest()->getParam('customer_data');
        $storeId = $this->getRequest()->getParam('storeId');
        //check email existed
        try {
            $checkCustomer = $this->customerRepository->get($customerData['email']);
            $websiteId     = $checkCustomer->getWebsiteId();

            if ($this->customerHelper->isCustomerInStore($websiteId, $storeId)) {
                throw new \Exception(__('A customer with the same email already exists in an associated website.'));
            }
        }
        catch (\Exception $e) {
            // CustomerRepository will throw exception if can't not find customer with email
        }

        try {
            $customerCreateData = [
              'firstname' => $customerData['firstname'],
              'lastname' => $customerData['lastname'],
              'email' => $customerData['email'],
              'gender' => $customerData['gender'],
              'dob' => isset($customerData['dob']) ? $customerData['dob'] : null
            ];

            $customerDataObject = $this->customerInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $customerDataObject,
                $customerCreateData,
                \Magento\Customer\Api\Data\CustomerInterface::class
            );

            $store = $this->storeManager->getStore($storeId);

            $customerDataObject->setGroupId(
                $this->customerGroupManagement->getDefaultGroup($store->getId())->getId()
            );

            $customerDataObject->setWebsiteId($store->getWebsiteId());
            $customerDataObject->setStoreId($store->getId());

            //check password confirmation
            $password = $customerData['password'];
            $password_confirmation = $customerData['password_confirmation'];
            $this->checkPasswordConfirmation($password, $password_confirmation);

            $customer = $this->accountManagement
                ->createAccount($customerDataObject, $password);

            //subscription
            if (isset($customerData['is_subscribed']) && $customer->getId()) {
                if ($customerData['is_subscribed']) {
                    $this->subscriberFactory->create()->subscribeCustomerById($customer->getId());
                } else {
                    $this->subscriberFactory->create()->unsubscribeCustomerById($customer->getId());
                }
            }

            //email confirmation
            $confirmation_required = false;
            $confirmationStatus = $this->accountManagement->getConfirmationStatus($customer->getId());
            if ($confirmationStatus === \Magento\Customer\Api\AccountManagementInterface::ACCOUNT_CONFIRMATION_REQUIRED) {
                $confirmation_required = true;
            }

            $searchCriteria = new \Magento\Framework\DataObject(
                [
                    'storeId'   => $storeId,
                    'entity_id' => $customer->getId(),
                    'confirmation_required' => $confirmation_required,
                    'email' => $customerData['email'],
                ]);

            return $this->loadDataCustomer($searchCriteria)->getOutput();

        }  catch (LocalizedException $e) {
            throw $e;
        }


    }

    /**
     * @param $password
     * @param $confirmation
     * @throws InputException
     */
    protected function checkPasswordConfirmation($password, $confirmation)
    {
        if ($password != $confirmation) {
            throw new InputException(__('Please make sure your passwords match.'));
        }
    }

    /**
     * @param $searchCriteria
     * @return \SM\Core\Api\SearchResult
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function loadDataCustomer($searchCriteria) {

        $websiteId = $this->storeManager->getStore($searchCriteria['storeId'])->getWebsiteId();
        $customer = $this->customerRegistry->retrieveByEmail($searchCriteria['email'], $websiteId);

        $customers = [];

        /** @var $customerModel \Magento\Customer\Model\Customer */
        $xCustomer = new XCustomer();
        $xCustomer->addData($customer->getData());
        $xCustomer->setData('tax_class_id', $customer->getTaxClassId());

        $xCustomer->setData('address', $this->getCustomerAddress($customer));

        $checkSubscriber = $this->subscriberFactory->create()->loadByCustomerId($customer->getId());
        if ($checkSubscriber->isSubscribed()) {
            $xCustomer->setData('subscription', true);
        } else {
            $xCustomer->setData('subscription', false);
        }

        if ($this->integrateHelper->isIntegrateRP() && $this->integrateHelper->isAHWRewardPoints()) {
            $xCustomer->setData('reward_point', $this->integrateHelper->getRpIntegrateManagement()
                                                                      ->getCurrentIntegrateModel()
                                                                      ->getCurrentPointBalance(
                                                                          $customer->getEntityId(),
                                                                          $this->storeManager->getStore($searchCriteria['storeId'])->getWebsiteId()));
        }

        $customers[] = $xCustomer;

        return $this->getSearchResult()
            ->setSearchCriteria($searchCriteria)
            ->setItems($customers)
            ->setLastPageNumber(1)
            ->setTotalCount(1);
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     *
     * @return array
     */
    protected function getCustomerAddress(\Magento\Customer\Model\Customer $customer) {
        $customerAdd = [];

        foreach ($customer->getAddresses() as $address) {
            /** @var \Magento\Customer\Model\Address $address */
            $customerAdd[] = $this->getAddressData($address);
        }

        return $customerAdd;
    }

    /**
     * Get customer address base on api
     *
     * @param \Magento\Customer\Model\Address $address
     *
     * @return array
     */
    protected function getAddressData(\Magento\Customer\Model\Address $address) {
        $addData           = $address->getData();
        $addData['street'] = $address->getStreet();
        $_customerAdd      = new CustomerAddress($addData);

        return $_customerAdd->getOutput();
    }


    /**
     * @return array|bool
     * @throws SecurityViolationException
     * @throws \Exception
     */
    public function resetPasswordViaPWA() {
        $email = (string)$this->getRequest()->getParam('email');

        try {
            $this->accountManagement->initiatePasswordReset(
                $email,
                \Magento\Customer\Model\AccountManagement::EMAIL_RESET
            );

        } catch (NoSuchEntityException $exception) {
            // Do nothing, we don't want anyone to use this action to determine which email accounts are registered.
        } catch (SecurityViolationException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw $exception;
        }

        $items = [];
        $items[] = ['success' => true];

        return $this->getSearchResult()
            ->setItems($items)
            ->setLastPageNumber(1)
            ->setTotalCount(1)
            ->getOutput();
    }
}
