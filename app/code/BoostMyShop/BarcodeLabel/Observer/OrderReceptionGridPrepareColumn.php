<?php

namespace BoostMyShop\BarcodeLabel\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class OrderReceptionGridPrepareColumn implements ObserverInterface
{

    public function execute(EventObserver $observer){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $config = $objectManager->create('BoostMyShop\Supplier\Model\Config');
        $barcodeAttribute = $config->getBarcodeAttribute();

        $grid = $observer->getEvent()->getgrid();
        if($barcodeAttribute)
        {
            $grid->addColumn('print_barcode',
                ['filter' => false,
                    'sortable' => false,
                    'align' => 'center',
                    'header' => __('Barcode'),
                    'index' =>$barcodeAttribute,
                    'renderer' => '\BoostMyShop\BarcodeLabel\Block\Order\Edit\Tab\Reception\Renderer\PrintBarcode'
                ]);

        }

        return $grid;


    }

}


