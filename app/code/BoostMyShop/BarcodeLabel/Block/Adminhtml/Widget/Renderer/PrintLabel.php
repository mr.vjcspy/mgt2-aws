<?php

namespace BoostMyShop\BarcodeLabel\Block\Adminhtml\Widget\Renderer;

use Magento\Framework\DataObject;
use Magento\Store\Model\StoreManagerInterface;

class PrintLabel extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    public function render(DataObject $row)
    {
        $url = $this->getUrl('barcodelabel/product/printLabel', ['qty' => 'param_qty', 'id' => $row->getId()]);
        $html = '<input type="button" value="'.__('Print').'" onclick="printBarcodeLabel(\''.$url.'\')">';

        return $html;
    }
}