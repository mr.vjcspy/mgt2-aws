<?php

namespace BoostMyShop\BarcodeLabel\Block\Rewrite\Order\Edit\Tab\Products\Renderer;

use Magento\Framework\DataObject;

class Barcode extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    public function render(DataObject $row)
    {
        $html = '';
        $html .= '<a data-qty="'.$row->getPendingQty().'" data-product-id = "'.(int)$row->getpop_product_id().'" class="bacode-poitem" href="javascript:void(0)">'.$row->getProduct()->getData($this->getColumn()->getIndex()).'</a>';
        return $html;
    }
}
