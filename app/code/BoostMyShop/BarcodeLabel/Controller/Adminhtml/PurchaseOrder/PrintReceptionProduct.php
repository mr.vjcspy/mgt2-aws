<?php

namespace BoostMyShop\BarcodeLabel\Controller\Adminhtml\PurchaseOrder;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action\Context;


class PrintReceptionProduct extends \Magento\Backend\App\AbstractAction
{
    protected $productFactory;
    protected $_filesystem;


    public function __construct(
        Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Filesystem $filesystem
    ) {
        $this->productFactory = $productFactory;
        $this->_filesystem = $filesystem;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_auth->getAuthStorage()->setIsFirstPageAfterLogin(false);

        $products = [];

        $productId = $this->getRequest()->getParam('product_id');
        $qty = $this->getRequest()->getParam('qty');

        if(!$qty || $qty <= 0)
        {
            $this->messageManager->addErrorMessage(__("Qty is required field."));
            $this->_redirect('supplier/order/receive', ["po_id" => $this->getRequest()->getParam('po_id')]);
            return;
        }

        $product = $this->productFactory->create()->load($productId);
        $products[] = ['product' => $product, 'qty' => $qty];

        try{
            $pdf = $this->_objectManager->create('BoostMyShop\BarcodeLabel\Model\Pdf')->getPdf($products);
            $this->_objectManager->get('\Magento\Framework\App\Response\Http\FileFactory')->create(
                'barcode_label_' . $product->getId() . '.pdf',
                $pdf->render(),
                DirectoryList::VAR_DIR,
                'application/pdf'
            );

            //delete file
            $dir = $this->_filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
            $dir->delete('barcode_label_' . $product->getId() . '.pdf');

        }catch(\Exception $e){
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_redirect('supplier/order/receive', ["po_id" => $this->getRequest()->getParam('po_id')]);
        }
    }

    protected function _isAllowed()
    {
        return true;
    }
}
