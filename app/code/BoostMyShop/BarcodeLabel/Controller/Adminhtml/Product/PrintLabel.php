<?php

namespace BoostMyShop\BarcodeLabel\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Controller\Adminhtml\Product\Builder as ProductBuilder;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class PrintLabel extends \Magento\Backend\App\AbstractAction
{
    protected $_label;
    protected $_filesystem;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder
     */
    public function __construct(
        Context $context,
        ProductBuilder $productBuilder,
        \Magento\Framework\Filesystem $filesystem
    ) {
        $this->productBuilder = $productBuilder;
        $this->_filesystem = $filesystem;

        parent::__construct($context);
    }

    public function execute()
    {
        $count = (int)$this->getRequest()->getParam('qty');
        $product = $this->productBuilder->build($this->getRequest());
        $fileName = 'barcode_label_' . $product->getName() . '.pdf';

        $products[] = ['product' => $product, 'qty' => $count];

        try {
            $pdf = $this->_objectManager->create('BoostMyShop\BarcodeLabel\Model\Pdf')->getPdf($products);
            $this->_objectManager->get('\Magento\Framework\App\Response\Http\FileFactory')->create(
                $fileName,
                $pdf->render(),
                DirectoryList::VAR_DIR,
                'application/pdf'
            );

            //Delete file
            $dir = $this->_filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
            $dir->delete($fileName);

        }catch(\Exception $e){
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_redirect('*/*/index');
        }

    }

    protected function _isAllowed()
    {
        return true;
    }

}
