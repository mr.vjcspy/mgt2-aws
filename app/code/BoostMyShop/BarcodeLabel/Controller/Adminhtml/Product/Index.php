<?php

namespace BoostMyShop\BarcodeLabel\Controller\Adminhtml\Product;

class Index extends \Magento\Backend\App\AbstractAction
{


    /**
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Barcode Label'));
        $this->_view->renderLayout();
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

}
