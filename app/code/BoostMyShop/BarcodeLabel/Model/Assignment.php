<?php

namespace BoostMyShop\BarcodeLabel\Model;

class Assignment
{
    protected $_config;
    protected $_productCollectionFactory;
    protected $_productAction;

    /*
     * @var \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \BoostMyShop\BarcodeLabel\Model\ConfigFactory $config,
        \Magento\Catalog\Model\Product\Action $productAction,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ){
        $this->_config = $config;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productAction = $productAction;
        $this->_productRepository = $productRepository;
    }

    /**
     * Create a barcode value for every products without barcode
     */
    public function assignForAllProducts()
    {
        $barcodeAttribute = $this->_config->create()->getBarcodeAttribute();
        $collection = $this->_productCollectionFactory->create()->addAttributeToSelect($barcodeAttribute);
        $productCount = $collection->getSize();
        $productIds = $collection->getAllIds();

	 $processedWithBarcode = 0;
	 $processedWithoutBarcode = 0;
	 $processedCount = 0;
	 $processedPercent = 0;
		
        foreach($productIds as $productId)
        {
            if(php_sapi_name() == "cli")
                echo "Products already having barcode : $processedWithBarcode | New barcode(s) assigned : $processedWithoutBarcode | Progress : $processedPercent % \r";
			
            $product = $this->getProductById($productId);

            if (!$product->getData($barcodeAttribute)){
                $this->assignForOneProduct($product->getId());
                $processedWithoutBarcode++;
            } else {
	         $processedWithBarcode++;
	    }
			
            $processedCount ++;
            $processedPercent = (int)($processedCount / $productCount * 100);
        }
    }

    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }
	
    /**
     * @param $productId
     */
    public function assignForOneProduct($productId)
    {
        $barcodeAttribute = $this->_config->create()->getBarcodeAttribute();
        $barcode = $this->generateBarcodeForProduct($productId);
        $this->_productAction->updateAttributes([$productId], [$barcodeAttribute => $barcode], 0);
    }

    public function generateBarcodeForProduct($productId)
    {
        $prefix = '99';
        $core = str_pad($productId, 10, '0', 0);
        $controlKey = $this->getControlKey($prefix.$core);
        $barcode = $prefix.$core.$controlKey;
        return $barcode;
    }

    /**
     * Return EAN13 control key
     *
     * @param $core
     * @return int
     */
    protected function getControlKey($core)
    {
        $sum = 0;

        for ($index = 0; $index < strlen($core); $index++) {
            $number = (int) $core[$index];
            if (($index % 2) != 0)
                $number *= 3;
            $sum += $number;
        }

        $resteDivision = $sum % 10;

        if ($resteDivision == 0)
            return 0;
        else
            return 10 - $resteDivision;
    }
}
