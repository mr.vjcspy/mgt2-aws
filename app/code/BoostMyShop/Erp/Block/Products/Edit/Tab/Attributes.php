<?php

namespace BoostMyShop\Erp\Block\Products\Edit\Tab;

class Attributes extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    protected $_advancedStockConfig;
    protected $_orderPreparationConfig;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \BoostMyShop\AdvancedStock\Model\Config $advancedStockConfig,
        \BoostMyShop\OrderPreparation\Model\Config $orderPreparationConfig,
        array $data = []
    ) {

        parent::__construct($context, $registry, $formFactory, $data);

        $this->_advancedStockConfig = $advancedStockConfig;
        $this->_orderPreparationConfig = $orderPreparationConfig;
    }

    public function getProduct()
    {
        return $this->_coreRegistry->registry('current_product');
    }

    protected function _prepareForm()
    {

        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('supplier_');

        $baseFieldset = $form->addFieldset('base_fieldset', ['legend' => __('Attributes')]);

        if ($this->_advancedStockConfig->getBarcodeAttribute()) {
            $baseFieldset->addField(
                'barcode',
                'text',
                [
                    'name' => 'barcode',
                    'label' => __('Barcode'),
                    'id' => 'barcode',
                    'title' => __('Barcode'),
                    'value' => $this->getProduct()->getData($this->_advancedStockConfig->getBarcodeAttribute())
                ]
            );
        }

        if ($this->_orderPreparationConfig->getMpnAttribute())
        {
            $baseFieldset->addField(
                'mpn',
                'text',
                [
                    'name' => 'mpn',
                    'label' => __('MPN'),
                    'id' => 'mpn',
                    'title' => __('MPN'),
                    'value' => $this->getProduct()->getData($this->_orderPreparationConfig->getMpnAttribute())
                ]
            );
        }

        $baseFieldset->addField(
            'weight',
            'text',
            [
                'name' => 'weight',
                'label' => __('Weight'),
                'id' => 'weight',
                'title' => __('Weight'),
                'value' => $this->getProduct()->getData('weight')
            ]
        );

        if ($this->_orderPreparationConfig->getVolumeAttribute())
        {
            $baseFieldset->addField(
                'volume',
                'text',
                [
                    'name' => 'volume',
                    'label' => __('Volume'),
                    'id' => 'volume',
                    'title' => __('Volume'),
                    'value' => $this->getProduct()->getData($this->_orderPreparationConfig->getVolumeAttribute())
                ]
            );
        }

        if ($this->_orderPreparationConfig->getPackageNumberAttribute()) {
            $baseFieldset->addField(
                'package_number',
                'text',
                [
                    'name' => 'package_number',
                    'label' => __('Package number'),
                    'id' => 'package_number',
                    'title' => __('Package number'),
                    'value' => $this->getProduct()->getData($this->_orderPreparationConfig->getPackageNumberAttribute())
                ]
            );
        }

        $baseFieldset->addField(
            'discontinued',
            'select',
            [
                'name' => 'discontinued',
                'label' => __('Discontinued'),
                'id' => 'discontinued',
                'title' => __('Discontinued'),
                'options' => ['1' => __('Yes'), '0' => __('No')],
                'value' => $this->getProduct()->getData('supply_discontinued')
            ]
        );

        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function getTabLabel()
    {
        return __('Attributes');
    }

    public function getTabTitle()
    {
        return __('Attributes');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

}
