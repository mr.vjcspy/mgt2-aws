<?php

namespace BoostMyShop\Erp\Controller\Adminhtml\Products;

class Save extends \BoostMyShop\Erp\Controller\Adminhtml\Products
{
    /**
     * @return void
     */
    public function execute()
    {
        $id = $this->getRequest()->getPost('id');
        $product = $this->_productFactory->create()->load($id);
        $this->_coreRegistry->register('current_product', $product);

        try
        {
            //save attributes
            $postData = $this->getRequest()->getPost();
            if (isset($postData['barcode']))
                $product->setData($this->_advancedStockConfig->getBarcodeAttribute(), $postData['barcode']);
            if (isset($postData['mpn']))
                $product->setData($this->_orderPreparationConfig->getMpnAttribute(), $postData['mpn']);
            if (isset($postData['volume']))
                $product->setData($this->_orderPreparationConfig->getVolumeAttribute(), $postData['volume']);
            if (isset($postData['weight']))
                $product->setData('weight', $postData['weight']);
            if (isset($postData['package_number']))
                $product->setData($this->_orderPreparationConfig->getPackageNumberAttribute(), $postData['package_number']);
            if (isset($postData['discontinued']))
                $product->setData('supply_discontinued', $postData['discontinued']);

            $product->save();

			$this->_eventManager->dispatch('erp_product_edit_save', ['product' => $product,  'post_data' => $this->getRequest()->getPost(), 'message_manager' => $this->messageManager]);
			
            $this->messageManager->addSuccess(__('Product details saved.'));
        }
        catch(\Exception $ex)
        {
            $this->messageManager->addSuccess(__('An error occured : '.$ex->getMessage()));
        }

        $this->_redirect('erp/products/edit', ['id' => $id]);

    }
}
