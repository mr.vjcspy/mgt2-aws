<?php

namespace BoostMyShopCustom\BarcodeLabelButton\Controller\Adminhtml\Product;


use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Response\Http\FileFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use BoostMyShop\BarcodeLabel\Model\Pdf;

class MassPrintLabels extends \Magento\Backend\App\AbstractAction
{
    protected $_label;
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    protected $productRepository;
    protected $_stockState;
    protected $_filesystem;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var Pdf
     */
    protected $pdf;

    /**
     * @param \Magento\Backend\App\Action\Context                   $context
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder
     */
    public function __construct(
        Context $context,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\CatalogInventory\Api\StockStateInterface $stockState,
        Filter $filter,
        CollectionFactory $collectionFactory,
        \Magento\Framework\Filesystem $filesystem,
        Pdf $pdf,
        FileFactory $fileFactory
    ) {
        $this->productRepository = $productRepository;
        $this->filter            = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->_stockState       = $stockState;
        $this->_filesystem       = $filesystem;
        $this->pdf               = $pdf;
        $this->fileFactory       = $fileFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        $products   = [];
        $printQty   = [];
        $productIds = [];

        $data = array_map(
            function ($item) { return explode(':', $item); },
            explode(';', $this->getRequest()->getParam('data'))
        );
        foreach ($data as $item) {
            if(isset($item[0]) && !empty($item[0])){
                $productIds[]       = intval($item[0]);
                $printQty[$item[0]] = intval($item[1]);
            }
        }

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->collectionFactory->create()->addFieldToFilter('entity_id', ['in' => $productIds]);
        /** @var \Magento\Catalog\Api\Data\ProductInterface $product */
        foreach ($collection->getItems() as $product) {
            $product = $this->productRepository->getById($product->getId());
            $qty     = $printQty[$product->getId()];

            $products[] = ['product' => $product, 'qty' => $qty];
        }

        try {
            $pdf      = $this->pdf->getPdf($products);
            $fileName = 'barcode_label_' . $product->getId() . '.pdf';

            $this->fileFactory->create(
                $fileName,
                $pdf->render(),
                DirectoryList::VAR_DIR,
                'application/pdf'
            );

            //Delete file
            $dir = $this->_filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
            $dir->delete($fileName);

        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_redirect('*/*/index');
        }

        $this->endExecute();
    }

    protected function endExecute()
    {
        exit(0);
    }

}
