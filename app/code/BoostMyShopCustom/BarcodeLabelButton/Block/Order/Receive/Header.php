<?php

namespace BoostMyShopCustom\BarcodeLabelButton\Block\Order\Receive;

use BoostMyShop\BarcodeLabel\Model\Config;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Module\Manager;

class Header extends \Magento\Backend\Block\Template
{

    protected $_coreRegistry       = null;
    protected $_barcodeLableConfig = null;

    /**
     * @var Manager
     */
    private $_moduleManager;

    /**
     * Header constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry             $registry
     * @param array                                   $data
     * @param Manager                                 $moduleManager
     * @param Config                                  $barcodeLableConfig
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = [],
        Manager $moduleManager,
        Config $barcodeLableConfig
    ) {
        parent::__construct($context, $data);
        $this->_moduleManager      = $moduleManager ?: ObjectManager::getInstance()->get(Manager::class);
        $this->_barcodeLableConfig = $barcodeLableConfig ?: ObjectManager::getInstance()->get(Config::class);
        $this->_coreRegistry       = $registry;
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {

        return $this->getUrl('*/*/edit', ['po_id' => $this->_coreRegistry->registry('current_purchase_order')->getId()]);
    }

    /**
     * @param $moduleName
     *
     * @return bool
     */
    public function isModuleEnabled($moduleName)
    {
        return $this->_moduleManager->isEnabled($moduleName) && $this->_barcodeLableConfig->isEnabled();
    }
}
