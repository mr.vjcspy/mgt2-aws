<?php

namespace BoostMyShopCustom\POCustomization\Block;

class Products extends \BoostMyShop\Supplier\Block\Order\Edit\Tab\Products
{
    private $sortOrder;

    protected function _afterLoadCollection()
    {
        $collection = $this->getCollection();
        $collection = $this->_customsorting($collection);
        parent::setCollection($collection);
        return $this;
    }

    /**
     * @param $collection
     * @return mixed
     */
    protected function _customsorting($collection)
    {
        $this->sortOrder = [
            'XS' => 0, 'S' => 1, 'M' => 2, 'L' => 3, 'XL' => 4, 'XXL' => 5, '4' => 6, '6' => 7, '8' => 8, '10' => 9, '12' => 10, '14' => 11, '16' => 12, '18' => 13, '2' => 14, 'O/S' => 15, '20' => 16
        ];
        $collection->setOrder('pop_sku', 'ASC');
        $lastKey=null;
        $sorted=[];
        foreach ($collection->getItems() as $id => $item) {
            $sku = $item->getPopSku();
            foreach ($this->sortOrder as $rule => $int) {
                if (preg_match('%-' . $rule . '%', $sku)) {
                    $key = str_replace('-' . $rule, '', $sku);
//                    if ($lastKey && $key != $lastKey) {
//                    }
                    $sorted[$key][$int]=$id;
                    $lastKey = $key;
                    continue 2;
                }
            }
        }
        foreach ($sorted as $key => $value) {
            for ($i=0;$i<16;$i++) {
                if (isset($value[$i])) {
                    $item = $collection->getItemById($value[$i]);
                    $collection->removeItemByKey($value[$i]);
                    $collection->addItem($item);
                }
            }
        }

        return $collection;
    }
}
